var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;

// docker container run -d -p 5671:5671 -p 5672:5672 -p 15671:15671 -p 15672:15672 --name vnlp-rabbit RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password  rabbitmq:3-management